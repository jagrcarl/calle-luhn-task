package se.experis;

import java.util.Scanner;

public class Program {

     static int checkLuhn(String cardNo) { //the method to check if the ui is a correct luhn series
         int n=0, sum = 0, checkDigit = 0;
         String lastDigitString = cardNo.substring(cardNo.length() - 1); //takes the last character from ui and stores it
         int lastDigit = Integer.parseInt(lastDigitString); //store the last character as an int.
         String remainingDigits = cardNo.substring(0, cardNo.length() - 1); //stores every digit except the last
         boolean alternate = false; //this is used so that every other number can be used
         for (int i = cardNo.length() - 1; i >= 0; i--) {
             n = Integer.parseInt(cardNo.substring(i, i + 1));
             if (alternate == true) { //calculation for every other number. the calculation begins from the end, jumps over the last digit and then take every other number according to the luhns algorithm
                 n *= 2;
                 if (n > 9) {
                     n = (n % 10) + 1;
                 }
             }
             sum += n; //add the new value from n to sum
             checkDigit = sum;

             alternate = !alternate; //changes the boolean statement
         }
         checkDigit = (checkDigit - lastDigit); //this is the calculation for the check digit
         checkDigit = (checkDigit * 9);
         checkDigit = (checkDigit % 10);

         if ((sum % 10) == 0) { //this statement is if the digits is correct according to luhns algoritm
             System.out.println("Input: " + remainingDigits + " " + lastDigit); //show the ui, but separate the last digit
             System.out.println("Provided: " + lastDigitString); //the last digit
             System.out.println("Expected: " + checkDigit); //the calcuted excepted last digit
             System.out.println("Checksum: Valid"); //if the digits is correct according to luhn algorithm
             if (cardNo.length() == 16) { // shows if the total nr of digit is 16 or not
                 System.out.println("Digits: " + cardNo.length() + " (credit card)");
             } else {
                 System.out.println("Digits: " + cardNo.length() + " (not a credit card)");
             }
         } else { //this statement is if the digits is not correct according to luhns algoritm
             System.out.println("Input: " + remainingDigits + " " + lastDigitString);
             System.out.println("Provided: " + lastDigitString);
             System.out.println("Expected: " + checkDigit);
             System.out.println("Checksum: Invalid");
             if (cardNo.length() == 16) {
                 System.out.println("Digits: " + cardNo.length() + " (credit card)");
             } else {
                 System.out.println("Digits: " + cardNo.length() + " (not a credit card)");
             }
         }
         return checkDigit;
     }

    static public void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Type your numbers: ");
        String cardNo = scan.nextLine(); //stores the ui
        checkLuhn(cardNo);
    }
}

