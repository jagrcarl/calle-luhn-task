package se.experis;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProgramTest {

    @Test
    void checkLuhnFalse() {
        assertNotEquals(0, Program.checkLuhn("123456"));
        assertNotEquals(1, Program.checkLuhn("123456"));
        assertNotEquals(2, Program.checkLuhn("123456"));


    }

    @Test
    void checkLuhnTrue() {
        assertEquals(5, Program.checkLuhn("123456"));
        assertEquals(6, Program.checkLuhn("1234567"));
        assertEquals(2, Program.checkLuhn("4242424242424242"));

    }

    @Test
    void checkLuhnNotNull() {
        Program.checkLuhn("123456");
        Program.checkLuhn("4242424242424242");

    }

    @Test
    void checkLuhnNull() {
        assertThrows(NullPointerException.class,
                () -> {
                    Program.checkLuhn(null);
                });
    }

    @Test
    void checkLuhnNoInput() {
        assertThrows(StringIndexOutOfBoundsException.class,
                () -> {
                    Program.checkLuhn("");
                });
    }
}